#define _USE_MATH_DEFINES
#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#include <vector>

#if defined(__APPLE__)
#include <GLUT/GLUT.h>
#include <OpenGL/gl3.h>
#include <OpenGL/glu.h>
#else
#if defined(WIN32) || defined(_WIN32) || defined(__WIN32__)
#include <windows.h>
#endif
#include <GL/glew.h>		// must be downloaded 
#include <GL/freeglut.h>	// must be downloaded unless you have an Apple
#endif

const unsigned int windowWidth = 800, windowHeight = 800;

// OpenGL major and minor versions

int majorVersion = 3, minorVersion = 0;

// vertex shader in GLSL
const char *phongVS = R"(	
    #version 130
    precision highp float;

    uniform mat4 M, Minv, MVP;
	uniform vec4  wLiPos;       // pos of light source 
	uniform vec3  wEye;         // pos of eye

	in  vec3 vtxPos;            // pos in modeling space
	in  vec3 vtxNorm;           // normal in modeling space

	out vec3 wNormal;           // normal in world space
	out vec3 wView;             // view in world space
	out vec3 wLight;            // light dir in world space

    void main() {
		gl_Position = vec4(vtxPos.x,vtxPos.y,vtxPos.z,1) * MVP;

		vec4 wPos = vec4(vtxPos, 1) * M;
		wLight  = wLiPos.xyz * wPos.w - wPos.xyz * wLiPos.w;
		wView   = wEye * wPos.w - wPos.xyz;
		wNormal = (Minv * vec4(vtxNorm, 0)).xyz;
	}
)";

// fragment shader in GLSL
const char *phongFS = R"(
	#version 130
    precision highp float;

	uniform vec3 kd, ks, ka;// diffuse, specular, ambient ref
	uniform vec3 La, Le;    // ambient and point source rad
	uniform float shine;    // shininess for specular ref

	in  vec3 wNormal;       // interpolated world sp normal
	in  vec3 wView;         // interpolated world sp view
	in  vec3 wLight;        // interpolated world sp illum dir
	out vec4 fragmentColor; // output goes to frame buffer
	void main ()  
	{     
		vec3 N = normalize(wNormal);
		vec3 V = normalize(wView);  
		vec3 L = normalize(wLight);
		vec3 H = normalize(L + V);
		float cost = max(dot(N,L), 0), cosd = max(dot(N,H), 0);
		vec3 color = ka * La + 
		           (kd * cost + ks * pow(cosd,shine)) * Le;
		fragmentColor = vec4(color, 1);
	} 
)";


const char * textureVS = R"(
    #version 130
    precision highp float;

    uniform mat4 M, Minv, MVP;
	uniform vec4  wLiPos;       // pos of light source 
	uniform vec3  wEye;         // pos of eye

	in  vec3 vtxPos;            // pos in modeling space
	in  vec3 vtxNorm;           // normal in modeling space
    in vec2 vtxUV;
   
    out vec2 texcoord;
	out vec3 wNormal;           // normal in world space
	out vec3 wView;             // view in world space
	out vec3 wLight;            // light dir in world space

    void main() {
        gl_Position = vec4(vtxPos, 1) * MVP; // to NDC
       
        texcoord = vtxUV;
        vec4 wPos = vec4(vtxPos, 1) * M;
        wLight  = wLiPos.xyz * wPos.w - wPos.xyz * wLiPos.w;
        wView   = wEye * wPos.w - wPos.xyz;
        wNormal = (Minv * vec4(vtxNorm, 0)).xyz;
    }
)";
 
const char * textureFS = R"(
    #version 130
    precision highp float;

	uniform vec3 kd, ks, ka;// diffuse, specular, ambient ref
	uniform vec3 La, Le;    // ambient and point source rad
	uniform float shine;    // shininess for specular ref
    uniform sampler2D textureUnit;     

    in vec2 texcoord;
	in  vec3 wNormal;       // interpolated world sp normal
	in  vec3 wView;         // interpolated world sp view
	in  vec3 wLight;        // interpolated world sp illum dir
	out vec4 fragmentColor; // output goes to frame buffer

    void main() {
        vec3 N = normalize(wNormal);
        vec3 V = normalize(wView);  
        vec3 L = normalize(wLight);
        vec3 H = normalize(L + V);
        float cost = max( dot(N,L), 0), cosd = max(dot(N,H), 0);
        vec3 color = texture(textureUnit, texcoord).xyz * La + (kd * cost + ks * pow(cosd,shine)) * Le;
        
        fragmentColor = vec4(color, 1);
    }
)";

struct mat4;

// row-major matrix 4x4
struct mat4 {
	float m[4][4];
public:
	mat4() {}
	mat4(float m00, float m01, float m02, float m03,
		float m10, float m11, float m12, float m13,
		float m20, float m21, float m22, float m23,
		float m30, float m31, float m32, float m33) {
		m[0][0] = m00; m[0][1] = m01; m[0][2] = m02; m[0][3] = m03;
		m[1][0] = m10; m[1][1] = m11; m[1][2] = m12; m[1][3] = m13;
		m[2][0] = m20; m[2][1] = m21; m[2][2] = m22; m[2][3] = m23;
		m[3][0] = m30; m[3][1] = m31; m[3][2] = m32; m[3][3] = m33;
	}

	operator float*() { return &m[0][0]; }

	mat4 operator*(const mat4& right) {
		mat4 result;
		for (int i = 0; i < 4; i++) {
			for (int j = 0; j < 4; j++) {
				result.m[i][j] = 0;
				for (int k = 0; k < 4; k++) result.m[i][j] += m[i][k] * right.m[k][j];
			}
		}
		return result;
	}

	mat4 operator+(const mat4& right) {
		mat4 result;
		for (int i = 0; i < 4; i++) {
			for (int j = 0; j < 4; j++) {
				result.m[i][j] = m[i][j] + right.m[i][j];
			}
		}
		return result;
	}

	mat4 operator *(const float& right) { 
		mat4 result;
		for (int i = 0; i < 4; i++) {
			for (int j = 0; j < 4; j++) {
				result.m[i][j] =  m[i][j] * right;
			}
		}
		return result;
	}
};

struct vec3 {
    float x, y, z;
 
    vec3(float xx = 0, float yy = 0, float zy = 0) {
        x = xx; y = yy; z = zy;
    }
    operator float*() { return &x; }

    vec3 operator*(float m) {
        return vec3(x * m, y * m, z * m);
    }
    vec3 operator+(const vec3& vec) {
        return vec3(x + vec.x, y + vec.y, z + vec.z);
    }
    vec3 operator-(const vec3& vec) {
        return vec3(x - vec.x, y - vec.y, z - vec.z);
    }
    vec3 operator*(const vec3& vec) {
        return vec3(x * vec.x, y * vec.y, z * vec.z);
    }
    vec3 operator/(const vec3& vec) {
        return vec3(x / vec.x, y / vec.y, z / vec.z);
    }
    vec3 operator+(float m) {
        return vec3(x + m, y + m, z + m);
    }
    vec3 operator-(float m) {
        return vec3(x - m, y - m, z - m);
    }

    vec3 operator*=(const vec3& vec) {
      *this = *this * vec;
	  return *this;
    }
    float length() {
        return sqrtf(x * x + y * y + z * z);
    }
    vec3 normalize() {
        float len = length();
        return vec3(x / len, y / len, z / len);
    }
};


// 3D point in homogeneous coordinates
struct vec4 {
	float x , y, z, w;

	vec4(float xx = 0, float yy = 0, float zz = 0, float ww = 1) {
		x = xx; y= yy; z = zz; w = ww;
	}

	vec4(const vec4& vec) {
		x = vec.x; y= vec.y; z = vec.z; w = vec.w;
	}

	 operator float*() { return &x; }

	vec4 operator*(const mat4& mat) {
		float result[4];
		float v[4] = {x,y,z,w};
		for (int j = 0; j < 4; j++) {
			result[j] = 0;
			for (int i = 0; i < 4; i++) result[j] += v[i] * mat.m[i][j];
		}
		return vec4(result[0], result[1], result[2], result[3]);
	}

	vec4 operator+ (const vec4& vec) const{
	 return vec4(x + vec.x, y+ vec.y, z + vec.z,w + vec.w );
	}

	vec4 operator+ (float m) const{
	 return vec4(x + m, y+ m, z + m, w + m);
	}

	vec4 operator+= (const vec4& vec) {
	  *this = *this + vec;
	  return *this;
	}

	vec4 operator- (const vec4& vec) const{
	  return vec4(x-vec.x, y- vec.y, z- vec.z,w - vec.w );
	} 

	vec4 operator- (float m) const{
	  return vec4(x - m, y- m, z - m, w - m );
	}
	

	vec4 operator-= (vec4& vec) {
	  *this = *this - vec;
	  return *this;
	}

	vec4 operator* (vec4& vec) {
	 return vec4(x*vec.x, y* vec.y, z* vec.z,w * vec.w );
	} 

	vec4 operator*= (vec4& vec) {
	  *this = *this * vec;
	  return *this;
	}

	vec4 operator* (float m) const{
	  return vec4(x * m, y* m, z * m, w * m );
	}

	vec4 operator*= (float m) {
	  *this = *this * m;
	  return *this;
	}

	vec4 operator/ (const vec4& vec) const{
	 return vec4(x/vec.x, y/ vec.y, z/ vec.z, w / vec.w );
	} 


	vec4 operator/ (float d) const{
	 return vec4(x/d, y/ d, z/d, w / d );
	}

	vec4 cross(const vec4& vec) const{
		return vec4(y*vec.z - z*vec.y, 
					z*vec.x - x*vec.z,
					x*vec.y- y*vec.x,
					1);
	}

	float dot(vec4& vec) const{
		return x*vec.x + y*vec.y+ z*vec.z;
	}

	float length() const{
		return sqrtf(x*x + y*y+ z*z);
	}

	float sum(){
		return (x + y + z);
	}

	vec4 normalize() const{
		vec4 result = *this / this->length();
		return result;
	}

	vec4 sin(float m){
		return vec4(sinf(x), sinf(y), sinf(z), sinf(w));
	}

	vec4 cos(float m){
		return vec4(cosf(x), cosf(y), cosf(z), cosf(w));
	}

};

float dot(const vec4& a ,const vec4& b){
		return a.x*b.x + a.y*b.y+ a.z*b.z;
	}

vec4 cross(const vec4& a, const vec4& b) {
	return vec4(b.y*a.z - b.z*a.y, 
				b.z*a.x - b.x*a.z,
				b.x*a.y- b.y*a.x,
				1);
}

vec4 operator* (const vec4& rhs, const vec4& lhs) {
	return vec4(rhs.x*lhs.x, rhs.y*lhs.y, rhs.z*lhs.z, rhs.w*lhs.w);
} 

int sign(float val) {
	return ((0.0f) < val) - (val < (0.0f));
}


mat4 TranslateMtx(const vec4& t) { 
   return mat4(1,   0,   0,   0,
               0,   1,   0,   0,
               0,   0,   1,   0,
              t.x, t.y, t.z,  1);
}
mat4 RotateMtx(float rotAngle, const vec4& rotAxis) {
	
	mat4 K(0, -rotAxis.z, rotAxis.y, 0,
		   rotAxis.z, 0, -rotAxis.x, 0,
		   -rotAxis.y, rotAxis.x, 0, 0,
			0, 0, 0, 0);
	mat4 I(1,   0,   0,   0,
           0,   1,   0,   0,
           0,   0,   1,   0,
           0,   0,   0,   1);

	return (I + K * (sinf(rotAngle)) + K * K * (1-cosf(rotAngle)) );  
}

mat4 ScaleMtx(const vec4& s) {
	return mat4(s.x,   0,   0,   0,
                  0, s.y,   0,   0,
                  0,   0, s.z,   0,
                  0,   0,   0,   1);
}

struct Light{
	vec3 La, Le, wLightPos;
	Light(){}
	Light(vec3 wLightPos,
		  vec3 La,
		  vec3 Le){
		this->wLightPos = wLightPos;
		this->La = La;
		this->Le = Le;
	}
};

struct Material{
    vec3 kd, ks, ka;
    float shininess;
 
    Material() {
        this->ka = vec3(0.5f, 0.5f, 0.5f);
        this->ks = vec3(0.5f, 0.5f, 0.5f);
        this->kd = vec3(1.0f, 1.0f, 1.0f);
        this->shininess = 10;
    }
    Material(vec3 kd, vec3 ks, vec3 ka, float shininess = 10) : kd(kd), ks(ks), ka(ka), shininess(shininess) {}
};



struct Camera {
   vec4  wEye, wLookat, wVup;
   float fov, asp, fp, bp;
public:
	Camera(const vec4&  wEye,const vec4& wLookat,const vec4& wVup){
		this->wEye = wEye;
		this->wLookat = wLookat;
		this->wVup = wVup;
        fov = 30.0f;
        fp = 0.1f;
        bp = 1000.0f;
        asp = 1;
	}
	Camera(const Camera& camera){
		wEye = camera.wEye;
		wLookat = camera.wLookat;
		wVup = camera.wVup;
		fov = camera.fov;
		asp = camera.asp;
		fp = camera.fp;
		bp = camera.bp;
	}
   mat4 V() { // view matrix
      vec4 w = (wEye - wLookat).normalize();
      vec4 u = cross(wVup, w).normalize();
      vec4 v = cross(w, u);
      return TranslateMtx(wEye * -1.0f) * mat4(u.x,  v.x,  w.x,  0.0f,
                                        u.y,  v.y,  w.y,  0.0f,
                                        u.z,  v.z,  w.z,  0.0f,
                                        0.0f, 0.0f, 0.0f, 1.0f);
   }
   mat4 P() { // projection matrix
      float sy = 1/tanf(fov/2);
      return mat4(sy/asp, 0.0f,  0.0f,               0.0f,
                  0.0f,   sy,    0.0f,               0.0f,
                  0.0f,   0.0f, -(fp+bp)/(bp - fp), -1.0f,
                  0.0f,   0.0f, -2*fp*bp/(bp - fp),  0.0f);
   }
};

struct Texture {
   unsigned int textureId, textureUnit;
   float* LoadImage(const char * fnam, int width, int height) {
   	float* image = new float[width*height*3];

   	float r[3] = {1,0,0};
   	float g[3] = {0,1,0};
   	float b[3] = {0,0,1};
   	float y[3] = {1,1,0};

   	float *cA = r; 
   	float *cB = y;
    for (int idx = 0; idx < width*height*3; idx+=3) {
    	int k = idx/3; if(idx/width%2 == 1) k -= 1;
        if (k % 2 == 0 )
        	for(int r = 0; r<3; r++) image[idx+r] = cA[r];
        else if (k % 2 == 1 ) 
        	for(int r = 0; r<3; r++) image[idx+r] = cB[r];
    }
          
   	return image;
   }
   Texture(){textureId = 0; textureUnit=0;}
   Texture(const char * fname) {
      textureUnit = 0;
      glGenTextures(1, &textureId);
      glBindTexture(GL_TEXTURE_2D, textureId);    // binding
      int width=16, height=16;
      float *image = LoadImage(fname,width,height); // megírni!
      glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, width, height, 
                   0, GL_RGB, GL_FLOAT, image); //Texture -> OpenGL
      glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
      glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
      delete(image);
  }
};


struct RenderState {
	mat4 M, V, P, Minv;
	vec4 wEye;
	Material material;
	Texture texture;
	Light light;
	unsigned int shader;
};

struct  Shader {
	unsigned int shader;

	void getErrorInfo(unsigned int handle) {
		int logLen;
		glGetShaderiv(handle, GL_INFO_LOG_LENGTH, &logLen);
		if (logLen > 0) {
			char * log = new char[logLen];
			int written;
			glGetShaderInfoLog(handle, logLen, &written, log);
			printf("Shader log:\n%s", log);
			delete log;
		}
	}

	// check if shader could be compiled
	void checkShader(unsigned int shader,const char * message) {
		int OK;
		glGetShaderiv(shader, GL_COMPILE_STATUS, &OK);
		if (!OK) {
			printf("%s!\n", message);
			getErrorInfo(shader);
		}
	}

	// check if shader could be linked
	void checkLinking(unsigned int program) {
		int OK;
		glGetProgramiv(program, GL_LINK_STATUS, &OK);
		if (!OK) {
			printf("Failed to link shader program!\n");
			getErrorInfo(program);
		}
	}

	void Create(const char * vcSrc, const char * vsAttrNames[],
				const char * fsSrc, const char * fsOutputName){
		// Create vertex shader from string
		unsigned int vertexShader = glCreateShader(GL_VERTEX_SHADER);
		if (!vertexShader) {
			printf("Error in vertex shader creation\n");
			exit(1);
		}	
		glShaderSource(vertexShader, 1, &vcSrc, NULL); glCompileShader(vertexShader);
		checkShader(vertexShader, "Vertex shader error");
		// Create fragment shader from string
		unsigned int fragmentShader = glCreateShader(GL_FRAGMENT_SHADER);
		if (!fragmentShader) {
			printf("Error in fragment shader creation\n");
			exit(1);
		}	
		glShaderSource(fragmentShader, 1, &fsSrc, NULL); glCompileShader(fragmentShader);
		checkShader(fragmentShader, "Fragment shader error");

		// Attach shaders to a single program
		shader = glCreateProgram();
		if (!shader) {
			printf("Error in shader program creation\n");
			exit(1);
		}
		glAttachShader(shader, vertexShader);
		glAttachShader(shader, fragmentShader);

		for (unsigned int i = 0; i < sizeof(vsAttrNames) / sizeof(char*); i++){
			glBindAttribLocation(shader, i, vsAttrNames[i]);
		}
		glBindFragDataLocation(shader, 0, fsOutputName);
		glLinkProgram(shader);
		checkLinking(shader);
	}
	virtual ~Shader(){
		glDeleteProgram(shader);
	}
	virtual void Bind(RenderState& state){
		glUseProgram(shader);
		mat4 MVP = state.M * state.V * state.P;
		glUniformMatrix4fv(glGetUniformLocation(shader,"MVP"),1,GL_TRUE, MVP);
	}
};

class PhongShader : public Shader{
public:
	PhongShader(){
		const char* vsAttrNames[] = {"vtxPos", "vtxNorm"};
		Create(phongVS, vsAttrNames, phongFS, "fragmentColor");
	}

	void Bind(RenderState& state){
		glUseProgram(shader);
		mat4 MVP = state.M * state.V * state.P;
		mat4 Minv = state.Minv, M = state.M;
		glUniformMatrix4fv(glGetUniformLocation(shader,"M"),1,GL_TRUE, M);
		glUniformMatrix4fv(glGetUniformLocation(shader,"Min	MAGYARORSZÁGI ZSIDÓ HITKÖZSÉGEK SZÖVETSÉGEv"),1,GL_TRUE,Minv); 
		glUniformMatrix4fv(glGetUniformLocation(shader,"MVP"),1,GL_TRUE, MVP);

		glUniform1f(glGetUniformLocation(shader, "shine"), state.material.shininess);
        glUniform3fv(glGetUniformLocation(shader, "ks"), 1, state.material.ks);
        glUniform3fv(glGetUniformLocation(shader, "kd"), 1, state.material.kd);
        glUniform3fv(glGetUniformLocation(shader, "ka"), 1, state.material.ka);
        glUniform3fv(glGetUniformLocation(shader, "La"), 1, state.light.La);
        glUniform3fv(glGetUniformLocation(shader, "Le"), 1, state.light.Le);
		glUniform4fv(glGetUniformLocation(shader, "wLiPos"), 1, state.light.wLightPos);
        glUniform3fv(glGetUniformLocation(shader, "wEye"), 1, state.wEye);
	}
};

class TextureShader : public Shader{
public:
	TextureShader(){
		const char* vsAttrNames[] = {"vtxPos", "vtxNorm", "vtxUV"};
		Create(textureVS, vsAttrNames, textureFS, "fragmentColor");
	}

	void Bind(RenderState& state){
		glUseProgram(shader);
		mat4 MVP = state.M * state.V * state.P;
		mat4 Minv = state.Minv, M = state.M;
		
		glUniformMatrix4fv(glGetUniformLocation(shader,"M"),1,GL_TRUE, M);
		glUniformMatrix4fv(glGetUniformLocation(shader,"Minv"),1,GL_TRUE,Minv); 
		glUniformMatrix4fv(glGetUniformLocation(shader,"MVP"),1,GL_TRUE, MVP);

		glUniform1f(glGetUniformLocation(shader, "shine"), state.material.shininess);
        glUniform3fv(glGetUniformLocation(shader, "ks"), 1, state.material.ks);
        glUniform3fv(glGetUniformLocation(shader, "kd"), 1, state.material.kd);
        glUniform3fv(glGetUniformLocation(shader, "ka"), 1, state.material.ka);
        glUniform3fv(glGetUniformLocation(shader, "La"), 1, state.light.La);
        glUniform3fv(glGetUniformLocation(shader, "Le"), 1, state.light.Le);
		glUniform4fv(glGetUniformLocation(shader, "wLiPos"), 1, state.light.wLightPos);
        glUniform3fv(glGetUniformLocation(shader, "wEye"), 1, state.wEye);

        glUniform1i(glGetUniformLocation(shader, "textureUnit"),  GL_TEXTURE0);
        glActiveTexture(GL_TEXTURE0);
        glBindTexture(GL_TEXTURE_2D, state.texture.textureId);
	}
};


struct VertexData {
   vec4 position, normal;
   float u, v;
   VertexData(){}
   VertexData(const VertexData& vd){
        this->position = vd.position;
        this->normal = vd.normal;
        this->u = vd.u;
        this->v = vd.v;
    }
};

struct Geometry {
 	unsigned int vao, nVtx;

  	Geometry(){ 
  		glGenVertexArrays(1, &vao); 
		glBindVertexArray(vao);
	}

	void Draw(RenderState state) {
		glBindVertexArray(vao); 
		glDrawArrays(GL_TRIANGLES, 0, nVtx);
	}
};

struct ParamSurface : Geometry {
   virtual VertexData GenVertexData(float u, float v) = 0;
   void Create(int N, int M) {
	   nVtx = 6* N * M;   
	   unsigned int vbo;
	   glGenBuffers(1, &vbo); glBindBuffer(GL_ARRAY_BUFFER, vbo);


	   VertexData *vtxData = new VertexData[nVtx], *pVtx = vtxData;
       for (int i = 0; i < N; i++) for (int j = 0; j < M; j++) {
            *pVtx++ = GenVertexData((float)i / N, (float)j / M);
            *pVtx++ = GenVertexData((float)(i + 1) / N, (float)j / M);
            *pVtx++ = GenVertexData((float)i / N, (float)(j + 1) / M);
            *pVtx++ = GenVertexData((float)(i + 1) / N, (float)j / M);
            *pVtx++ = GenVertexData((float)(i + 1) / N, (float)(j + 1) / M);
            *pVtx++ = GenVertexData((float)i / N, (float)(j + 1) / M);
        }
	        
	   int stride = sizeof(VertexData);
	   int sVec4 = sizeof(vec4); 
	   glBufferData(GL_ARRAY_BUFFER,
	   				nVtx * stride,
	   				vtxData,
	   				GL_STATIC_DRAW);

	   glEnableVertexAttribArray(0);  // AttribArray 0 = POSITION
	   glEnableVertexAttribArray(1);  // AttribArray 1 = NORMAL
	   glEnableVertexAttribArray(2);  // AttribArray 2 = UV
	   glVertexAttribPointer(0,4,GL_FLOAT, GL_FALSE, stride,(void*)0); 
	   glVertexAttribPointer(1,4,GL_FLOAT,GL_FALSE,stride,(void*)(intptr_t)sVec4);
	   glVertexAttribPointer(2,2,GL_FLOAT,GL_FALSE,stride,(void*)(intptr_t)(2*sVec4));
	   delete vtxData;
	}
};

class Sphere : public ParamSurface {
   vec4 center;
   float radius;
public:
   Sphere(vec4 c, float r, float res) : center(c), radius(r) {
	this->Create(res, res); // tessellation level
   }

   VertexData GenVertexData(float u, float v) {
      VertexData vd;
      vd.normal = vec4(cos(u*2*M_PI) * sin(v*M_PI),
                       sin(u*2*M_PI) * sin(v*M_PI),
                       cos(v*M_PI), 0);
      vd.position = vd.normal * radius + center;
      
      vd.u = u; vd.v = v;
      return vd;
   }
};

class Torus : public ParamSurface {
   vec4 center;
   float R, r;
public:
   Torus(vec4 c0, float R0, float r0, int res): center(c0), R(R0), r(r0){
        this->Create(res, res);
    }

   VertexData GenVertexData(float u, float v) {
       VertexData vd;
        vec4 a = vec4(R*cosf(2*M_PI*v),
        			  0,
        			  R*sinf(2*M_PI*v));
        vec4 b = vec4(r*cosf(2*M_PI*u)*cosf(2*M_PI*v),
        			  r*sinf(2*M_PI*u),
        			  r*cosf(2*M_PI*u)*sinf(2*M_PI*v));
        vd.normal = b.normalize();
        vd.position = a + b + center;
        vd.u = u; vd.v = v;
        return vd;
   }
};

struct Object {
   Shader *   shader;
   Material * material;
   Texture *  texture;
   Geometry*  geometry;
   vec4 scale, translation, rotAxis;
   float rotAngle;

   Object(){
   	shader = NULL;
   	geometry = NULL;
   	material = NULL;
   	texture = NULL;
   	scale = vec4(1.0f, 1.0f, 1.0f,0);
   	translation = vec4(0,0,2,0);
   	rotAxis = vec4(1,0,0,0);
   	rotAngle = 0;
   }

   Object(Shader* shader, Geometry* geometry, Material* material){
   	this->shader = shader;
   	this->geometry = geometry;
   	this->material = material;
   	texture = NULL;
   	scale = vec4(1.0f, 1.0f, 1.0f,0);
   	translation = vec4(0,0,0,0);
   	rotAxis = vec4(1,0,0,0);
   	rotAngle = 0;
   }
   Object(Shader* shader, Geometry* geometry, Material* material, Texture* texture){
   	this->shader = shader;
   	this->geometry = geometry;
   	this->material = material;
   	this->texture = texture;
   	scale = vec4(1.0f, 1.0f, 1.0f,0);
   	translation = vec4(0,0,0,0);
   	rotAxis = vec4(0,1,0,0);
   	rotAngle = 0;
   }
   virtual ~Object(){
   	if (shader != NULL)delete shader;
   	if (geometry != NULL)delete geometry;
   	if (material != NULL)delete material;
   	if (texture != NULL)delete texture;
   }

	void Draw(RenderState state) {	
	   state.M = ScaleMtx(scale) * RotateMtx(rotAngle, rotAxis) *
	             TranslateMtx(translation);
	   state.Minv = TranslateMtx(translation * -1.0f) * 
	                RotateMtx(-rotAngle, rotAxis) *
	                ScaleMtx(vec4(1/scale.x,1/scale.y,1/scale.z));
	    
	   if (material != NULL) state.material = *material; 
	   if (texture != NULL) state.texture = *texture;

	  shader->Bind(state);
	  geometry->Draw(state);

	}
	virtual void Animate(float t, float dt) {
		translation = vec4(sinf(t),cosf(t)*sinf(t),3+cosf(t)*0.8,0);
		rotAngle = t;
	}
};


struct Scene {
	Camera* camera; 
	std::vector<Object *> objects;
	std::vector<Light *> lights;
	RenderState state;
 
	Scene(Camera* camera){
		this->camera = camera;
		this->state = state;
	}
	~Scene(){
		for (unsigned int i = 0; i<objects.size(); i++) delete objects[i];
		for (unsigned int i = 0; i<lights.size(); i++) delete lights[i];
		delete camera;
	}
	void addObject(Object * obj){
		objects.push_back(obj);
	}
	void addLight(Light * lig){
		lights.push_back(lig);
	}
	void Render() {
		state.wEye = camera->wEye;
		state.light = *lights[0];
		state.V = camera->V(); 
		state.P = camera->P();
		for (Object * obj : objects) obj->Draw(state);
	}

	void Animate(float t, float dt) {
		for (Object * obj : objects) obj->Animate(t, dt);
	}
};


RenderState renderState;
Scene* scene;
Camera* camera;


void onInitialization() {
	glViewport(0, 0, windowWidth, windowHeight);
    glEnable(GL_DEPTH_TEST);
    glDisable(GL_CULL_FACE);

	///CAMERA
	camera = new Camera(vec4(0,0,0,0),vec4(0,0,1,0),vec4(0,1,0,0));
	///SCENE
	scene = new Scene(camera);
	//OBJECTS
	scene->addObject(new Object(new TextureShader,
							    new Sphere(vec4(0,0,0,0), 0.3f, 100),
							    new Material(),
							    new Texture("f")
							    ));

	scene->addObject(new Object(new TextureShader,
							    new Torus(vec4(0,0,0,0), 0.8f, 0.1f, 100),
							    new Material(),
							    new Texture("f")
								));


	//LIGHT
	scene->addLight(new Light(vec3(0,-2,0),
							  vec3(0.2,0.2,0.2),
							  vec3(1,1,1)));
}

void onExit() {
	printf("exit");
}

// Window has become invalid: Redraw
void onDisplay() {
	glClearColor(0, 0, 0, 0);							// background color 
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT); // clear the screen
	
	scene->Render();

	glutSwapBuffers();									// exchange the two buffers
}

// Key of ASCII code pressed
void onKeyboard(unsigned char key, int pX, int pY) {
	if (key == 'd') glutPostRedisplay();         // if d, invalidate display, i.e. redraw
}

// Key of ASCII code released
void onKeyboardUp(unsigned char key, int pX, int pY) {

}

// Mouse click event
void onMouse(int button, int state, int pX, int pY) {
	if (button == GLUT_LEFT_BUTTON && state == GLUT_DOWN) {  // GLUT_LEFT_BUTTON / GLUT_RIGHT_BUTTON and GLUT_DOWN / GLUT_UP
	}
}

// Move mouse with key pressed
void onMouseMotion(int pX, int pY) {
}

// Idle event indicating that some time elapsed: do animation here
void onIdle() {
	long time = glutGet(GLUT_ELAPSED_TIME); // elapsed time since the start of the program
    scene->objects[0]->Animate(time/200.0f, time);

    scene->objects[1]->Animate(time/500.0f, time);
    glutPostRedisplay();                    // redraw the scene
}

int main(int argc, char * argv[]) {
	glutInit(&argc, argv);
#if !defined(__APPLE__)
	glutInitContextVersion(majorVersion, minorVersion);
#endif
	glutInitWindowSize(windowWidth, windowHeight);				// Application window is initially of resolution 600x600
	glutInitWindowPosition(100, 100);							// Relative location of the application window
#if defined(__APPLE__)
	glutInitDisplayMode(GLUT_RGBA | GLUT_DOUBLE | GLUT_DEPTH | GLUT_3_2_CORE_PROFILE);  // 8 bit R,G,B,A + double buffer + depth buffer
#else
	glutInitDisplayMode(GLUT_RGBA | GLUT_DOUBLE | GLUT_DEPTH);
#endif
	glutCreateWindow(argv[0]);

#if !defined(__APPLE__)
	glewExperimental = true;	// magic
	glewInit();
#endif

	printf("GL Vendor    : %s\n", glGetString(GL_VENDOR));
	printf("GL Renderer  : %s\n", glGetString(GL_RENDERER));
	printf("GL Version (string)  : %s\n", glGetString(GL_VERSION));
	glGetIntegerv(GL_MAJOR_VERSION, &majorVersion);
	glGetIntegerv(GL_MINOR_VERSION, &minorVersion);
	printf("GL Version (integer) : %d.%d\n", majorVersion, minorVersion);
	printf("GLSL Version : %s\n", glGetString(GL_SHADING_LANGUAGE_VERSION));

	onInitialization();

	glutDisplayFunc(onDisplay);                // Register event handlers
	glutMouseFunc(onMouse);
	glutIdleFunc(onIdle);
	glutKeyboardFunc(onKeyboard);
	glutKeyboardUpFunc(onKeyboardUp);
	glutMotionFunc(onMouseMotion);

	glutMainLoop();
	onExit();
	return 1;
}
